<form action="{{ form.getAction() }}/" method="post">
    {% for element in form.getElements() %}
        {{ element.render() }}
    {% endfor %}
</form>

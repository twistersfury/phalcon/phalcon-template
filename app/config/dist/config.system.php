<?php
/**
 * Copyright (C) 2018 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

/**
 * System Configuration. Typically Application Specific Config
 */
return [
    'debug'       => TF_DEBUG ?: getenv('ENV_APPLICATION_ENV') === 'development' || TF_RUNNING_TESTS,
    'system'      => [
        'theme' => 'default',
        'title' => [
            'default'   => 'Default Title',
            'separator' => ' | '
        ],
        'defaultRoute' => [
            'controller' => 'general',
            'module'     => 'site'
        ],
    ],
];

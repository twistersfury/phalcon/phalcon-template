<?php
/**
 * Copyright (C) 2018 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

/**
 * This is the base configuration, intended to be compatible with the Phalcon Developer Tools
 */
return [
    'debug'       => true,
    'environment' => getenv('ENV_APPLICATION_ENV') ?: 'production',
    'application' => [
        'logDir'    => realpath(__DIR__ . '/../../docker/logs'),
        'cacheDir'  => realpath(__DIR__ . '/../../cache'),
        'docRoot'   => realpath($_SERVER['DOCUMENT_ROOT']),
        'modelsDir' => realpath(__DIR__ . '/../modules/Shared/Mvc/Model')
    ],
    'database' => [
        'adapter'    => 'mysql',
        'host'       => getenv('ENV_DATABASE_HOST'),
        'username'   => getenv('ENV_DATABASE_USER'),
        'password'   => getenv('ENV_DATABASE_PASS'),
        'dbname'     => getenv('ENV_DATABASE_NAME'),
        'persistent' => false,
        'options'    => [
            \PDO::ATTR_EMULATE_PREPARES  => false,
            \PDO::ATTR_STRINGIFY_FETCHES => false,
            \PDO::ERRMODE_EXCEPTION
        ]
    ]
];

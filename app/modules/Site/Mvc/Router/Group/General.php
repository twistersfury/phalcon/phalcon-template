<?php
/**
 * Copyright (C) 2018 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

namespace TwistersFury\Phalcon\Template\Site\Mvc\Router\Group;

use TwistersFury\Phalcon\Shared\Mvc\Router\Group\AbstractGroup;

class General extends AbstractGroup
{
}

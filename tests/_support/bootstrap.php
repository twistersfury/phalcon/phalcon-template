<?php
/**
 * Copyright (C) 2018 Twister's Fury.
 * Distributed under the MIT License (license terms are at http://opensource.org/licenses/MIT).
 */

$_ENV['ENV_RUNNING_TESTS'] = true;
unset($_ENV['ENV_RUNNING_UNIT_TESTS']);

require_once __DIR__ . '/../../vendor/twistersfury/phalcon-shared/phalcon_loader.php';
